package com.june.jobhunt.telstra.controller;

import org.junit.Test;

import static io.restassured.RestAssured.*;
import static io.restassured.matcher.RestAssuredMatchers.*;
import static org.hamcrest.Matchers.*;

public class PhoneNumberControllerTest {

    @Test
    public void test_statuscode_is_ok() {
        get("/phoneNumbers").then().statusCode(200);
    }

    @Test
    public void test_has_mobile_and_home_number_types() {
        get("/phoneNumbers").then().body("phoneNumberType", hasItems("WORK","MOBILE"));
    }

    @Test
    public void test_has_all_the_customers() {
        get("/phoneNumbers").then().body("customerId", hasItems(1,2));
    }

    @Test
    public void activate_phone_number() {
        post("/phoneNumbers/1/active/true");
        get("/phoneNumbers").then().body("active",hasItems(true));
    }
}
