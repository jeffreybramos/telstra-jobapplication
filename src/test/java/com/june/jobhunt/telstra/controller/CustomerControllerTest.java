package com.june.jobhunt.telstra.controller;

import org.junit.Test;
import static io.restassured.RestAssured.*;
import static io.restassured.matcher.RestAssuredMatchers.*;
import static org.hamcrest.Matchers.*;

public class CustomerControllerTest {

    @Test
    public void test_statuscode_is_ok() {
        get("/customers").then().statusCode(200);
    }

    @Test
    public void test_all_the_customers() {
        get("/customers").then().body("name", hasItems("John Doe","Meg Ryan"));
    }

    @Test
    public void test_phonenumbers_for_a_customer() {
        get("/customers/1/phoneNumbers").then().body("phoneNumber", hasItems("04535221","045352523"));
    }
}
