insert into customer (id, name) values (1, 'John Doe');

insert into phone_number (customer_id, phone_number_type, phone_number, active) values (1, 'MOBILE', '04535221',0);
insert into phone_number (customer_id, phone_number_type, phone_number, active) values (1, 'HOME', '0453525357',0);
insert into phone_number (customer_id, phone_number_type, phone_number, active) values (1, 'MOBILE', '043523632',0);
insert into phone_number (customer_id, phone_number_type, phone_number, active) values (1, 'MOBILE', '045352523',0);
insert into phone_number (customer_id, phone_number_type, phone_number, active) values (1, 'MOBILE', '045342325',0);

insert into customer (id, name) values (2, 'Meg Ryan');

insert into phone_number (customer_id, phone_number_type, phone_number, active) values (2, 'WORK', '045532532',0);
insert into phone_number (customer_id, phone_number_type, phone_number, active) values (2, 'MOBILE', '04534235',0);
insert into phone_number (customer_id, phone_number_type, phone_number, active) values (2, 'HOME', '04432225',0);
insert into phone_number (customer_id, phone_number_type, phone_number, active) values (2, 'MOBILE', '043532523',0);
insert into phone_number (customer_id, phone_number_type, phone_number, active) values (2, 'HOME', '045352237',0);