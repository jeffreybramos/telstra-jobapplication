package com.june.jobhunt.telstra.domain;

public enum PhoneNumberType {
    MOBILE, HOME, WORK;
}
