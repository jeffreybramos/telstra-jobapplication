package com.june.jobhunt.telstra.service;

import com.june.jobhunt.telstra.domain.Customer;
import com.june.jobhunt.telstra.domain.PhoneNumber;
import com.june.jobhunt.telstra.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerService {

    @Autowired
    private CustomerRepository customerRepository;

    public List<Customer> findAllCustomers() {
        return customerRepository.findAll();
    }

    public Customer findCustomer(Long id) {
        return customerRepository.findById(id).get();
    }

    public List<PhoneNumber> findCustomerPhoneNumbers(Long id) {
        return this.findCustomer(id).getPhoneNumbers();
    }
}
