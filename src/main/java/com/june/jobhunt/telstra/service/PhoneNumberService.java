package com.june.jobhunt.telstra.service;

import com.june.jobhunt.telstra.domain.PhoneNumber;
import com.june.jobhunt.telstra.repository.PhoneNumberRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class PhoneNumberService {

    @Autowired
    private PhoneNumberRepository phoneNumberRepository;

    public List<PhoneNumber> findAllPhoneNumbers() {
        return phoneNumberRepository.findAll();
    }

    @Transactional
    public Boolean activatePhoneNumber(Long phoneNumberId, Boolean active) {
        phoneNumberRepository.findById(phoneNumberId).get().setActive(active);
        return true;
    }
}
