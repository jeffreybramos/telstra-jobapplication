package com.june.jobhunt.telstra.controller;

import com.june.jobhunt.telstra.domain.Customer;
import com.june.jobhunt.telstra.domain.PhoneNumber;
import com.june.jobhunt.telstra.dto.CustomerDTO;
import com.june.jobhunt.telstra.dto.PhoneNumberDTO;
import com.june.jobhunt.telstra.service.CustomerService;
import com.june.jobhunt.telstra.util.PropertyCopier;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping(path = "/customers")
@Api(value = "Customer Resource", description = "API for Customer Resource")
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @Autowired
    private PropertyCopier propertyCopier;

    @GetMapping
    @ApiOperation(value = "Returns all customers")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 404, message = "Customer not found"),
                    @ApiResponse(code = 500, message = "Internal Server Error"),
                    @ApiResponse(code = 200, message = "Successful")
            }
    )
    public List<CustomerDTO> findAllCustomers() {
        List<Customer> allCustomers = customerService.findAllCustomers();
        List<CustomerDTO> allCustomerDTOs = new ArrayList<>();

        for (Customer customer : allCustomers) {

            CustomerDTO customerDTO = new CustomerDTO();

            propertyCopier.copyProperties(customer, customerDTO, Set.of("id", "name"));

            for (PhoneNumber phoneNumber : customer.getPhoneNumbers()) {
                PhoneNumberDTO phoneNumberDTO = new PhoneNumberDTO();
                propertyCopier.copyProperties(phoneNumber, phoneNumberDTO, Set.of("id", "phoneNumberType", "phoneNumber"));

                customerDTO.getPhoneNumbers().add(phoneNumberDTO);
            }

            allCustomerDTOs.add(customerDTO);
        }

        return allCustomerDTOs;
    }

    @GetMapping(path = "{id}/phoneNumbers")
    @ApiOperation(value = "Returns all phone numbers by a specific customer")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 404, message = "Customer not found"),
                    @ApiResponse(code = 500, message = "Internal Server Error"),
                    @ApiResponse(code = 200, message = "Successful")
            }
    )
    public List<PhoneNumberDTO> getCustomerPhoneNumbers(@PathVariable("id") Long id) {
        List<PhoneNumber> customerPhoneNumbers = customerService.findCustomerPhoneNumbers(id);
        List<PhoneNumberDTO> phoneNumberDTOs = new ArrayList<>();

        for(PhoneNumber phoneNumber : customerPhoneNumbers) {
            PhoneNumberDTO phoneNumberDTO = new PhoneNumberDTO();

            propertyCopier.copyProperties(phoneNumber, phoneNumberDTO, Set.of("id", "phoneNumberType", "phoneNumber", "customerId", "active"));
            phoneNumberDTOs.add(phoneNumberDTO);
        }

        return phoneNumberDTOs;
    }

}
