package com.june.jobhunt.telstra.controller;

import com.june.jobhunt.telstra.domain.PhoneNumber;
import com.june.jobhunt.telstra.dto.PhoneNumberDTO;
import com.june.jobhunt.telstra.service.PhoneNumberService;
import com.june.jobhunt.telstra.util.PropertyCopier;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping(path = "/phoneNumbers")
public class PhoneNumberController {

    @Autowired
    private PhoneNumberService phoneNumberService;

    @Autowired
    private PropertyCopier propertyCopier;

    @GetMapping
    @ApiOperation(value = "Returns all phone numbers")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 404, message = "Customer not found"),
                    @ApiResponse(code = 500, message = "Internal Server Error"),
                    @ApiResponse(code = 200, message = "Successful")
            }
    )
    public List<PhoneNumberDTO> findAllPhoneNumbers() {
        List<PhoneNumber> allPhoneNumbers = phoneNumberService.findAllPhoneNumbers();
        List<PhoneNumberDTO> phoneNumberDTOs = new ArrayList<>();

        for(PhoneNumber phoneNumber : allPhoneNumbers) {
            PhoneNumberDTO phoneNumberDTO = new PhoneNumberDTO();
            propertyCopier.copyProperties(phoneNumber, phoneNumberDTO, Set.of("id","phoneNumberType","phoneNumber","customerId", "active"));
            phoneNumberDTOs.add(phoneNumberDTO);
        }

        return phoneNumberDTOs;
    }

    @PostMapping(path = "{phoneNumberId}/active/{active}")
    public Boolean activatePhoneNumber(@PathVariable("phoneNumberId") Long phoneNumberId, @PathVariable("active") Boolean active) {
        return phoneNumberService.activatePhoneNumber(phoneNumberId, active);
    }
}
